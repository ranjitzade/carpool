package com.carpool.splash;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.carpool.CPApplication;
import com.carpool.auth.login.LoginActivity;
import com.carpool.home.HomeActivity;
import com.carpool.rides.create.CreateRidesActivity;

/**
 * Created by ranjit
 */
public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (CPApplication.getPreferences().isUserLoggedIn()) {
            CreateRidesActivity.start(this);
        } else {
            LoginActivity.start(this);
        }

    }
}

package com.carpool;

import android.app.Application;

import com.carpool.utils.CPPreferences;
import com.carpool.utils.logger.Log;
import com.google.firebase.FirebaseApp;

/**
 * Created by ranjit
 */
public class CPApplication extends Application {

    private static volatile CPApplication mContext;
    private static volatile CPPreferences mPreferences;

    public static CPPreferences getPreferences() {
        if (mPreferences == null) {
            synchronized (CPPreferences.class) {
                mPreferences = new CPPreferences(mContext);
            }
        }
        return mPreferences;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        FirebaseApp.initializeApp(this);
        mContext = this;
        Log.init(true);
    }
}

package com.carpool.auth.login;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;

import com.carpool.R;
import com.carpool.auth.AuthPresenter;
import com.carpool.auth.AuthView;
import com.carpool.auth.register.AuthError;
import com.carpool.auth.register.SignUpActivity;
import com.carpool.commons.CPActivity;
import com.carpool.rides.create.CreateRidesActivity;
import com.carpool.utils.UiUtils;
import com.carpool.utils.views.CPImageView;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import butterknife.BindView;
import butterknife.OnClick;
import butterknife.OnTextChanged;

/**
 * Created by ranjit
 */
public class LoginActivity extends CPActivity<AuthPresenter, AuthView> implements AuthView {

    @BindView(R.id.imageViewBg)
    protected CPImageView mImageViewBg;
    @BindView(R.id.textInputLayoutEmail)
    protected TextInputLayout mTextInputLayoutEmail;
    @BindView(R.id.editTextEmail)
    protected TextInputEditText mEditTextEmail;
    @BindView(R.id.textInputLayoutPassword)
    protected TextInputLayout mTextInputLayoutPassword;
    @BindView(R.id.editTextPassword)
    protected TextInputEditText mEditTextPassword;

    public static void start(Context context) {
        Intent starter = new Intent(context, LoginActivity.class);
        starter.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(starter);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_login;
    }

    @NonNull
    @Override
    public AuthPresenter providePresenter() {
        return new AuthPresenter();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mImageViewBg.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.login_background));
    }

    @OnClick(R.id.buttonLogin)
    protected void onButtonLoginClicked() {
        UiUtils.hideKeyboard(this, getCurrentFocus());
        if (mEditTextEmail.getText() == null) {
            setError(AuthError.EMAIL, R.string.invalid_email_address);
            return;
        }

        if (mEditTextPassword.getText() == null) {
            setError(AuthError.PASSWORD, R.string.invalid_password);
            return;
        }

        boolean isValid = getPresenter().checkEmailAndPasswordConditions(mEditTextEmail.getText().toString().trim(), mEditTextPassword.getText().toString().trim());
        if (isValid) {
            onPasswordTextChange();
            onEmailTextChange();
            String email = mEditTextEmail.getText().toString().trim();
            String password = mEditTextPassword.getText().toString().trim();
            getPresenter().loginUser(email, password);
        }
    }

    @OnClick(R.id.buttonSignUp)
    protected void onButtonSignUpClicked() {
        UiUtils.hideKeyboard(this, getCurrentFocus());
        SignUpActivity.start(this);
    }

    @OnTextChanged(value = R.id.editTextPassword, callback = OnTextChanged.Callback.AFTER_TEXT_CHANGED)
    protected void onPasswordTextChange() {
        mTextInputLayoutPassword.setError(null);
    }

    @OnTextChanged(value = R.id.editTextEmail, callback = OnTextChanged.Callback.AFTER_TEXT_CHANGED)
    protected void onEmailTextChange() {
        mTextInputLayoutEmail.setError(null);
    }

    @Override
    public void gotoNextScreen(int currentItem) {
    }

    @Override
    public void setError(int errorType, int errorRes) {
        switch (errorType) {
            case AuthError.EMAIL:
                mTextInputLayoutEmail.setError(getString(errorRes));
                break;
            case AuthError.PASSWORD:
                mTextInputLayoutPassword.setError(getString(errorRes));
                break;
            case AuthError.MOBILE:
            case AuthError.USERNAME:
        }
    }

    @Override
    public void onSuccess() {
        CreateRidesActivity.start(this);
    }
}

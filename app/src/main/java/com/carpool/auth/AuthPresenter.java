package com.carpool.auth;

import androidx.annotation.NonNull;

import com.carpool.CPApplication;
import com.carpool.R;
import com.carpool.auth.register.AuthError;
import com.carpool.commons.CPPresenter;
import com.carpool.models.User;
import com.carpool.utils.AppExecutors;
import com.carpool.utils.CPPreferences;
import com.carpool.utils.Constants;
import com.carpool.utils.StringUtils;
import com.carpool.utils.logger.Log;
import com.google.android.gms.tasks.OnCanceledListener;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import static com.carpool.utils.AppExecutors.getInstance;

/**
 * Created by ranjit
 */
public class AuthPresenter extends CPPresenter<AuthView> {

    private FirebaseDatabase mDatabase;
    private FirebaseAuth mAuth;
    private OnCompleteListener<AuthResult> mRegisterCompleteListener;
    private OnCanceledListener mRegisterOnCanceledListener;
    private OnFailureListener mRegisterOnFailureListener;

    @Override
    protected void onCreate() {
        super.onCreate();
        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance();
    }

    public boolean checkEmailAndPasswordConditions(String email, String password) {
        if (!StringUtils.isValidEmail(email)) {
            sendToView(authView -> authView.setError(AuthError.EMAIL, R.string.invalid_email_address));
            return false;
        }

        if (StringUtils.isEmpty(password)) {
            sendToView(authView -> authView.setError(AuthError.PASSWORD, R.string.invalid_password_length));
            return false;
        }
        return true;
    }

    public void checkEmailAndPasswordConditions(int currentItem, String email) {
        sendToView(authView -> authView.showProgressDialog(true));

        mAuth.fetchSignInMethodsForEmail(email).addOnCompleteListener(
                getInstance().networkIO(),
                task -> getInstance().mainThread().execute(() -> sendToView(authView -> {
                    authView.showProgressDialog(false);
                    if (task.isSuccessful()) {
                        if (task.getResult() == null
                                || task.getResult().getSignInMethods() == null
                                || task.getResult().getSignInMethods().size() <= 0) {
                            authView.gotoNextScreen(currentItem);
                            return;
                        }
                        authView.setError(AuthError.EMAIL, R.string.email_already_exists);
                    } else {
                        authView.gotoNextScreen(currentItem);
                    }
                })));
    }

    public void checkForUserNameConditions(int currentItem, String userName) {
        if (userName == null) {
            return;
        }

        sendToView(authView -> authView.showProgressDialog(true));
        mDatabase.getReference(Constants.Firebase.USERS)
                .orderByChild(Constants.Firebase.USERNAME)
                .equalTo(userName)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        Log.e();
                        sendToView(authView -> {
                            Log.e();
                            authView.showProgressDialog(false);
                            try {
                                Log.e();

                                User user = null;
                                for (DataSnapshot child : dataSnapshot.getChildren()) {
                                    User value = child.getValue(User.class);
                                    Log.e(value);
                                    if (value != null && userName.equals(value.username)) {
                                        user = value;
                                        break;
                                    }
                                }

                                Log.e(user);
                                if (user == null) {
                                    // this means the user is not present
                                    authView.gotoNextScreen(currentItem);
                                } else {
                                    authView.setError(AuthError.USERNAME, R.string.username_exists);
                                }
                            } catch (Exception ex) {
                                Log.e(ex.getMessage());
                                ex.printStackTrace();
                            }
                        });
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        Log.e(databaseError.getMessage());
                        Log.e(databaseError.getDetails());
                        sendToView(authView -> {
                            authView.showProgressDialog(false);
                            authView.gotoNextScreen(currentItem);
                        });
                    }
                });
    }

    public void checkForMobileNumber(int currentItem, String mobileNumber) {
        if (mobileNumber == null) {
            return;
        }
        sendToView(authView -> authView.showProgressDialog(true));
        mDatabase.getReference(Constants.Firebase.USERS)
                .orderByChild(Constants.Firebase.MOBILE_NUMBER)
                .equalTo(mobileNumber)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        sendToView(authView -> {
                            authView.showProgressDialog(false);
                            try {
                                User user = null;
                                for (DataSnapshot child : dataSnapshot.getChildren()) {
                                    User value = child.getValue(User.class);
                                    Log.e(value);
                                    if (value != null && mobileNumber.equals(value.mobileNumber)) {
                                        user = value;
                                        break;
                                    }
                                }
                                Log.e(user);

                                if (user == null) {
                                    // this means the user is not present
                                    authView.gotoNextScreen(currentItem);
                                } else {
                                    authView.setError(AuthError.MOBILE, R.string.mobile_number_exists);
                                }
                            } catch (Exception ex) {
                                ex.printStackTrace();
                                authView.gotoNextScreen(currentItem);
                            }
                        });
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        sendToView(authView -> {
                            authView.showProgressDialog(false);
                            authView.gotoNextScreen(currentItem);
                        });
                    }
                });

    }

    public void registerUser(User user, String password) {
        sendToView(authView -> authView.showProgressDialog(true));

        mRegisterCompleteListener = null;
        mRegisterOnCanceledListener = null;
        mRegisterOnFailureListener = null;
        mAuth.signOut();

        mRegisterCompleteListener = task -> {
            if (task.isSuccessful()) {

                // when sign-up is successful
                CPApplication.getPreferences().setLoggedIn(true);
                sendToView(authView -> {
                    authView.showProgressDialog(false);

                    if (task.getResult() != null && task.getResult().getUser() != null) {
                        FirebaseUser firebaseUser = task.getResult().getUser();
                        user.userUuid = firebaseUser.getUid();

                        DatabaseReference usersDb = FirebaseDatabase.getInstance().getReference().child(Constants.Firebase.USERS);
                        usersDb.child(user.userUuid).setValue(user);
                        authView.onSuccess();
                        task.getResult().getUser().sendEmailVerification()
                                .addOnCompleteListener(getInstance().networkIO(),
                                        task1 -> Log.d("email sent for verification"));
                    }

                });

            } else {
                CPApplication.getPreferences().setLoggedIn(false);
                sendToView(authView -> {
                    authView.showProgressDialog(false);
                    authView.showSnackBar(task.getException() != null ? task.getException().getMessage() : "Please try after sometime.");
                });
            }
        };

        mRegisterOnCanceledListener = () -> {
            CPApplication.getPreferences().setLoggedIn(false);
            sendToView(authView -> authView.showProgressDialog(false));
        };

        mRegisterOnFailureListener = e -> {
            CPApplication.getPreferences().setLoggedIn(false);
            sendToView(authView -> authView.showProgressDialog(false));

        };

        mAuth.createUserWithEmailAndPassword(user.email, password)
                .addOnCompleteListener(getInstance().networkIO(), mRegisterCompleteListener)
                .addOnCanceledListener(getInstance().networkIO(), mRegisterOnCanceledListener)
                .addOnFailureListener(getInstance().networkIO(), mRegisterOnFailureListener);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mRegisterCompleteListener = null;
        mRegisterOnCanceledListener = null;
        mRegisterOnFailureListener = null;
        mAuth = null;
        mDatabase = null;
    }

    public void loginUser(String email, String password) {
        sendToView(authView -> authView.showProgressDialog(true));
        mAuth.signInWithEmailAndPassword(email, password).addOnCompleteListener(AppExecutors.getInstance().networkIO(),
                task -> {
                    sendToView(authView -> authView.showProgressDialog(false));
                    if (task.isSuccessful()) {
                        if (task.getResult() == null || task.getResult().getUser() == null) {
                            return;
                        }

                        if (!task.getResult().getUser().isEmailVerified()) {
                            sendToView(authView -> authView.showSnackBar("Verify email address to login."));
                            return;
                        }

                        CPPreferences preferences = CPApplication.getPreferences();
                        preferences.setUserUuid(task.getResult().getUser().getUid());
                        preferences.setEmailId(email);
                        preferences.setLoggedIn(true);
                        sendToView(AuthView::onSuccess);
                    } else {
                        sendToView(authView -> authView.showSnackBar("Invalid Id or password"));
                    }
                });
    }
}
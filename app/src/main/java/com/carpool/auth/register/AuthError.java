package com.carpool.auth.register;

import androidx.annotation.IntDef;

import static com.carpool.auth.register.AuthError.EMAIL;
import static com.carpool.auth.register.AuthError.MOBILE;
import static com.carpool.auth.register.AuthError.PASSWORD;
import static com.carpool.auth.register.AuthError.USERNAME;

/**
 * Created by ranjit
 */
@IntDef({EMAIL, USERNAME, MOBILE, PASSWORD})
public @interface AuthError {
    int EMAIL = 0;
    int USERNAME = 1;
    int MOBILE = 2;
    int PASSWORD = 3;
}

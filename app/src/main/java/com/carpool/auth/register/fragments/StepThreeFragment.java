package com.carpool.auth.register.fragments;

import android.content.Context;
import android.widget.RadioGroup;

import androidx.annotation.NonNull;

import com.carpool.R;
import com.carpool.auth.register.NextButtonClickListener;
import com.carpool.commons.CPFragment;
import com.carpool.utils.DateUtils;
import com.carpool.utils.SnackBarUtils;
import com.carpool.utils.StringUtils;
import com.carpool.utils.views.CPImageView;
import com.google.android.material.datepicker.MaterialDatePicker;
import com.google.android.material.radiobutton.MaterialRadioButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import butterknife.BindView;
import butterknife.OnClick;
import butterknife.OnTextChanged;

/**
 * Created by ranjit
 */
public class StepThreeFragment extends CPFragment {
    private static final String TAG = "StepThreeFragment";

    @BindView(R.id.imageViewUser)
    protected CPImageView mImageViewUser;
    @BindView(R.id.textInputLayoutFullName)
    protected TextInputLayout mTextInputLayoutFullName;
    @BindView(R.id.editTextFullName)
    protected TextInputEditText mEditTextFullName;
    @BindView(R.id.textInputLayoutDateOfBirth)
    protected TextInputLayout mTextInputLayoutDateOfBirth;
    @BindView(R.id.editTextDateOfBirth)
    protected TextInputEditText mEditTextDateOfBirth;
    @BindView(R.id.textInputLayoutMobileNumber)
    protected TextInputLayout mTextInputLayoutMobileNumber;
    @BindView(R.id.editTextMobileNumber)
    protected TextInputEditText mEditTextMobileNumber;
    @BindView(R.id.textInputLayoutWorkPlace)
    protected TextInputLayout mTextInputLayoutWorkPlace;
    @BindView(R.id.editTextWorkPlace)
    protected TextInputEditText mEditTextWorkPlace;
    @BindView(R.id.textInputLayoutEducation)
    protected TextInputLayout mTextInputLayoutEducation;
    @BindView(R.id.editTextEducation)
    protected TextInputEditText mEditTextEducation;
    @BindView(R.id.textInputLayoutBio)
    protected TextInputLayout mTextInputLayoutBio;
    @BindView(R.id.editTextBio)
    protected TextInputEditText mEditTextBio;
    @BindView(R.id.radioButtonFemale)
    protected MaterialRadioButton mRadioButtonFemale;
    @BindView(R.id.radioButtonMale)
    protected MaterialRadioButton mRadioButtonMale;
    @BindView(R.id.radioGroupGender)
    protected RadioGroup mRadioGroupGender;

    private NextButtonClickListener mNextButtonClickListener;
    private long selection = 0;

    public static StepThreeFragment newInstance() {
        return new StepThreeFragment();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof NextButtonClickListener) {
            mNextButtonClickListener = (NextButtonClickListener) context;
        }
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_step_three;
    }

    @Override
    protected void onViewCreated() {
    }

    @OnClick({R.id.editTextDateOfBirth, R.id.textInputLayoutDateOfBirth})
    protected void onDateOfBirthClicked() {
        MaterialDatePicker<Long> datePicker = MaterialDatePicker.Builder.datePicker().setTitleTextResId(R.string.dob).build();
        datePicker.addOnPositiveButtonClickListener(selection -> {
            String dob = DateUtils.getDateInString(selection);
            mEditTextDateOfBirth.setText(dob);
            this.selection = selection;
        });

        datePicker.show(getChildFragmentManager(), TAG);
    }

    @OnClick(R.id.buttonNext)
    protected void onNextButtonClicked() {
        if (mEditTextFullName.getText() == null) {
            mTextInputLayoutFullName.setError(getString(R.string.invalid_full_name));
            return;
        }

        String fullName = mEditTextFullName.getText().toString().trim();
        if (StringUtils.isEmpty(fullName)) {
            mTextInputLayoutFullName.setError(getString(R.string.invalid_full_name));
            return;
        }

        if (mEditTextMobileNumber.getText() == null) {
            mTextInputLayoutMobileNumber.setError(getString(R.string.invalid_mobile_number));
            return;
        }

        String mobileNumber = mEditTextMobileNumber.getText().toString().trim();
        if (StringUtils.isEmpty(mobileNumber) || mobileNumber.length() != 10) {
            mTextInputLayoutMobileNumber.setError(getString(R.string.invalid_mobile_number_length));
            return;
        }

        if (mRadioGroupGender.getCheckedRadioButtonId() != R.id.radioButtonMale
                && mRadioGroupGender.getCheckedRadioButtonId() != R.id.radioButtonFemale) {
            SnackBarUtils.showSnackBar(getView(), "Please select gender");
            return;
        }

        if (mNextButtonClickListener != null) {
            mNextButtonClickListener.onNextButtonClicked();
        }
    }

    @OnTextChanged(R.id.editTextFullName)
    protected void onFullNameTextChange() {
        mTextInputLayoutFullName.setError(null);
    }

    @OnTextChanged(R.id.editTextDateOfBirth)
    protected void onDobTextChange() {
        mTextInputLayoutDateOfBirth.setError(null);
    }

    @OnTextChanged(R.id.editTextMobileNumber)
    protected void onMobileNumberTextChange() {
        mTextInputLayoutMobileNumber.setError(null);
    }

    public String getMobileNumber() {
        if (mEditTextMobileNumber.getText() == null) {
            return null;
        }
        return mEditTextMobileNumber.getText().toString().trim();
    }

    public String getFullName() {
        if (mEditTextFullName.getText() == null) {
            return null;
        }
        return mEditTextFullName.getText().toString().trim();
    }

    public String getDob() {
        if (mEditTextDateOfBirth.getText() == null) {
            return null;
        }
        return mEditTextDateOfBirth.getText().toString().trim();
    }

    public String getWorkPlace() {
        if (mEditTextWorkPlace.getText() == null) {
            return null;
        }
        return mEditTextWorkPlace.getText().toString().trim();
    }

    public String getEducation() {
        if (mEditTextEducation.getText() == null) {
            return null;
        }
        return mEditTextEducation.getText().toString().trim();
    }

    public String getBio() {
        if (mEditTextBio.getText() == null) {
            return null;
        }
        return mEditTextBio.getText().toString().trim();
    }


    public void setMobileNumberError(String error) {
        mTextInputLayoutMobileNumber.setError(error);
    }

    public String getGender() {
        return mRadioGroupGender.getCheckedRadioButtonId() == R.id.radioButtonFemale ? "Female" : "Male";
    }
}
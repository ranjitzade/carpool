package com.carpool.auth.register.fragments;

import android.content.Context;
import android.text.Editable;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.carpool.R;
import com.carpool.auth.register.NextButtonClickListener;
import com.carpool.commons.CPFragment;
import com.carpool.utils.StringUtils;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import butterknife.BindView;
import butterknife.OnClick;
import butterknife.OnTextChanged;

/**
 * Created by ranjit
 */
public class StepOneFragment extends CPFragment {
    @BindView(R.id.textInputLayoutEmail)
    protected TextInputLayout mTextInputLayoutEmail;
    @BindView(R.id.editTextEmail)
    protected TextInputEditText mEditTextEmail;
    @BindView(R.id.textInputLayoutPassword)
    protected TextInputLayout mTextInputLayoutPassword;
    @BindView(R.id.editTextPassword)
    protected TextInputEditText mEditTextPassword;

    @Nullable
    private NextButtonClickListener mNextButtonClickListener;

    public static StepOneFragment newInstance() {
        return new StepOneFragment();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof NextButtonClickListener) {
            mNextButtonClickListener = (NextButtonClickListener) context;
        }
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_step_one;
    }

    @Override
    protected void onViewCreated() {
    }

    @OnClick(R.id.buttonNext)
    protected void onNextClicked() {
        if (mEditTextEmail.getText() == null) {
            mTextInputLayoutEmail.setError(getString(R.string.invalid_email_address));
            return;
        }

        String email = mEditTextEmail.getText().toString().trim();
        if (!StringUtils.isValidEmail(email)) {
            mTextInputLayoutEmail.setError(getString(R.string.invalid_email_address));
            return;
        }

        if (mEditTextPassword.getText() == null) {
            mTextInputLayoutPassword.setError(getString(R.string.invalid_password_length));
            return;
        }

        String password = mEditTextPassword.getText().toString().trim();
        if (password.length() < 6) {
            mTextInputLayoutPassword.setError(getString(R.string.invalid_password_length));
            return;
        }

        if (mNextButtonClickListener != null) {
            mNextButtonClickListener.onNextButtonClicked();
        }
    }

    @OnTextChanged(value = R.id.editTextEmail, callback = OnTextChanged.Callback.AFTER_TEXT_CHANGED)
    protected void emailTextChangeListener(Editable editable) {
        mTextInputLayoutEmail.setError(null);
    }

    @OnTextChanged(value = R.id.editTextPassword, callback = OnTextChanged.Callback.AFTER_TEXT_CHANGED)
    protected void passwordTextChangeListener(Editable editable) {
        mTextInputLayoutPassword.setError(null);
    }

    public String getEmail() {
        if (mEditTextEmail.getText() == null) {
            return null;
        }
        return mEditTextEmail.getText().toString().trim();
    }

    public void setEmailError(String error) {
        mTextInputLayoutEmail.setError(error);
    }

    public String getPassword() {
        if (mEditTextPassword.getText() == null) {
            return null;
        }
        return mEditTextPassword.getText().toString().trim();
    }
}

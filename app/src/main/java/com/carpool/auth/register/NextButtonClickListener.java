package com.carpool.auth.register;

/**
 * Created by ranjit
 */
public interface NextButtonClickListener {
    void onNextButtonClicked();
}

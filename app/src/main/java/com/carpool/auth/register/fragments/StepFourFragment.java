package com.carpool.auth.register.fragments;

import android.content.Context;
import android.text.InputFilter;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.RadioGroup;

import androidx.annotation.NonNull;

import com.carpool.R;
import com.carpool.auth.register.NextButtonClickListener;
import com.carpool.commons.CPFragment;
import com.carpool.utils.SnackBarUtils;
import com.carpool.utils.StringUtils;
import com.google.android.material.radiobutton.MaterialRadioButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import butterknife.BindView;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;
import butterknife.OnTextChanged;

/**
 * Created by ranjit
 */
public class StepFourFragment extends CPFragment {
    @BindView(R.id.radioGroupVehicle)
    protected RadioGroup mRadioGroupVehicle;
    @BindView(R.id.radioButtonNo)
    protected MaterialRadioButton mRadioButtonNo;
    @BindView(R.id.radioButtonYes)
    protected MaterialRadioButton mRadioButtonYes;
    @BindView(R.id.linearLayoutVehicleDetails)
    protected LinearLayout mLinearLayoutVehicleDetails;
    @BindView(R.id.textInputLayoutCar)
    protected TextInputLayout mTextInputLayoutCar;
    @BindView(R.id.editTextCar)
    protected TextInputEditText mEditTextCar;
    @BindView(R.id.textInputLayoutRegistrationPlate)
    protected TextInputLayout mTextInputLayoutRegistrationPlate;
    @BindView(R.id.editTextRegistrationPlate)
    protected TextInputEditText mEditTextRegistrationPlate;
    @BindView(R.id.textInputLayoutSeats)
    protected TextInputLayout mTextInputLayoutSeats;
    @BindView(R.id.editTextSeats)
    protected TextInputEditText mEditTextSeats;

    private NextButtonClickListener mNextButtonClickListener;

    public static StepFourFragment newInstance() {
        return new StepFourFragment();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof NextButtonClickListener) {
            mNextButtonClickListener = (NextButtonClickListener) context;
        }
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_step_four;
    }

    @Override
    protected void onViewCreated() {
        mEditTextRegistrationPlate.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
    }

    @OnClick(R.id.buttonSubmit)
    protected void onSubmitClicked() {
        if (mRadioGroupVehicle.getCheckedRadioButtonId() != R.id.radioButtonYes
                && mRadioGroupVehicle.getCheckedRadioButtonId() != R.id.radioButtonNo) {
            SnackBarUtils.showSnackBar(getView(), getString(R.string.radio_check_vehicle));
            return;
        }

        if (mRadioGroupVehicle.getCheckedRadioButtonId() == R.id.radioButtonNo) {
            if (mNextButtonClickListener != null) {
                mNextButtonClickListener.onNextButtonClicked();
            }
            return;
        }

        if (mEditTextCar.getText() == null) {
            mTextInputLayoutCar.setError(getString(R.string.invalid_car_model));
            return;
        }

        String car = mEditTextCar.getText().toString().trim();
        if (StringUtils.isEmpty(car)) {
            mTextInputLayoutCar.setError(getString(R.string.invalid_car_model));
            return;
        }

        if (mEditTextRegistrationPlate.getText() == null) {
            mTextInputLayoutCar.setError(getString(R.string.invalid_registration_plate));
            return;
        }

        String registrationPlate = mEditTextRegistrationPlate.getText().toString().trim();
        if (StringUtils.isEmpty(registrationPlate)) {
            mTextInputLayoutCar.setError(getString(R.string.invalid_registration_plate));
            return;
        }

        if (mNextButtonClickListener != null) {
            mNextButtonClickListener.onNextButtonClicked();
        }
    }

    @OnTextChanged(value = R.id.editTextRegistrationPlate, callback = OnTextChanged.Callback.AFTER_TEXT_CHANGED)
    protected void onRegistrationTextChange() {
        mTextInputLayoutRegistrationPlate.setError(null);
    }

    @OnTextChanged(value = R.id.editTextCar, callback = OnTextChanged.Callback.AFTER_TEXT_CHANGED)
    protected void onCarModelTextChange() {
        mTextInputLayoutCar.setError(null);
    }

    @OnCheckedChanged({R.id.radioButtonYes, R.id.radioButtonNo})
    protected void onVehicleSelected(CompoundButton button, boolean checked) {
        if (getContext() == null) {
            return;
        }

        if (checked) {
            switch (button.getId()) {
                case R.id.radioButtonYes:
                    if (mLinearLayoutVehicleDetails.getVisibility() != View.VISIBLE) {
                        mLinearLayoutVehicleDetails.setVisibility(View.VISIBLE);
                    }
                    break;
                case R.id.radioButtonNo:
                    if (mLinearLayoutVehicleDetails.getVisibility() != View.GONE) {
                        mLinearLayoutVehicleDetails.setVisibility(View.GONE);
                    }
                    break;
            }
        } else {
            switch (button.getId()) {
                case R.id.radioButtonYes:
                    if (mLinearLayoutVehicleDetails.getVisibility() != View.GONE) {
                        mLinearLayoutVehicleDetails.setVisibility(View.GONE);
                    }
                    break;
                case R.id.radioButtonNo:
                    if (mLinearLayoutVehicleDetails.getVisibility() != View.VISIBLE) {
                        mLinearLayoutVehicleDetails.setVisibility(View.VISIBLE);
                    }
                    break;
            }
        }
    }

    public boolean getIsCarOwner() {
        return mRadioGroupVehicle.getCheckedRadioButtonId() == R.id.radioButtonYes;
    }

    public String getCarModel() {
        if (mEditTextCar.getText() == null) {
            return null;
        }
        return mEditTextCar.getText().toString().trim();
    }

    public String getRegistrationPlate() {
        if (mEditTextRegistrationPlate.getText() == null) {
            return null;
        }
        return mEditTextRegistrationPlate.getText().toString().trim();
    }

    public int getSeats() {
        if (mEditTextSeats.getText() == null) {
            return getIsCarOwner() ? 3 : 0;
        }
        String seats = mEditTextSeats.getText().toString().trim();
        if (StringUtils.isEmpty(seats)) {
            return 0;
        }
        return Integer.parseInt(seats);
    }

}
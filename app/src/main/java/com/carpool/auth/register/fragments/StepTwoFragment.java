package com.carpool.auth.register.fragments;

import android.content.Context;

import androidx.annotation.NonNull;

import com.carpool.R;
import com.carpool.auth.register.NextButtonClickListener;
import com.carpool.commons.CPFragment;
import com.carpool.utils.StringUtils;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import butterknife.BindView;
import butterknife.OnClick;
import butterknife.OnTextChanged;

/**
 * Created by ranjit
 */
public class StepTwoFragment extends CPFragment {
    @BindView(R.id.textInputLayoutUserName)
    protected TextInputLayout mTextInputLayoutUserName;
    @BindView(R.id.editTextUserName)
    protected TextInputEditText mEditTextUserName;

    private NextButtonClickListener mNextButtonClickListener;

    public static StepTwoFragment newInstance() {
        return new StepTwoFragment();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof NextButtonClickListener) {
            mNextButtonClickListener = (NextButtonClickListener) context;
        }
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_step_two;
    }

    @Override
    protected void onViewCreated() {
    }

    @OnClick(R.id.buttonNext)
    protected void onNextButtonClicked() {
        if (mEditTextUserName.getText() == null) {
            mTextInputLayoutUserName.setError(getString(R.string.invalid_username));
            return;
        }

        String username = mEditTextUserName.getText().toString().trim();
        if (StringUtils.isEmpty(username)) {
            mTextInputLayoutUserName.setError(getString(R.string.invalid_username));
            return;
        }

        if (username.contains(" ")) {
            mTextInputLayoutUserName.setError(getString(R.string.username_contains_spaces));
            return;
        }

        if (mNextButtonClickListener != null) {
            mNextButtonClickListener.onNextButtonClicked();
        }
    }

    public void setUserNameError(String error) {
        mTextInputLayoutUserName.setError(error);
    }

    @OnTextChanged(value = R.id.editTextUserName, callback = OnTextChanged.Callback.AFTER_TEXT_CHANGED)
    protected void userNameTextChangeListener() {
        mTextInputLayoutUserName.setError(null);
    }

    public String getUserName() {
        if (mEditTextUserName.getText() == null) {
            return null;
        }

        return mEditTextUserName.getText().toString().trim();
    }
}
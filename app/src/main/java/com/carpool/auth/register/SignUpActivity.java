package com.carpool.auth.register;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.carpool.R;
import com.carpool.auth.AuthPresenter;
import com.carpool.auth.AuthView;
import com.carpool.auth.login.LoginActivity;
import com.carpool.auth.register.fragments.StepFourFragment;
import com.carpool.auth.register.fragments.StepOneFragment;
import com.carpool.auth.register.fragments.StepThreeFragment;
import com.carpool.auth.register.fragments.StepTwoFragment;
import com.carpool.commons.CPActivity;
import com.carpool.commons.CPFragmentPagerAdapter;
import com.carpool.models.User;
import com.carpool.utils.AppExecutors;
import com.carpool.utils.SnackBarUtils;
import com.carpool.utils.UiUtils;
import com.carpool.utils.views.NonSwipeableViewPager;

import butterknife.BindView;

/**
 * Created by ranjit
 */
public class SignUpActivity extends CPActivity<AuthPresenter, AuthView> implements AuthView, NextButtonClickListener {
    @BindView(R.id.view_pager)
    protected NonSwipeableViewPager mViewPager;
    // register fragments
    private StepOneFragment mStepOneFragment;
    private StepTwoFragment mStepTwoFragment;
    private StepThreeFragment mStepThreeFragment;
    private StepFourFragment mStepFourFragment;

    public static void start(Context context) {
        Intent starter = new Intent(context, SignUpActivity.class);
        context.startActivity(starter);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_sign_up;
    }

    @NonNull
    @Override
    public AuthPresenter providePresenter() {
        return new AuthPresenter();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setToolbar(getString(R.string.register), true);
        if (toolbar != null) {
            toolbar.setNavigationOnClickListener(view -> {
                int currentItem = mViewPager.getCurrentItem();
                switch (currentItem) {
                    case 0:
                        onBackPressed();
                        return;
                    case 1:
                    case 2:
                    case 3:
                        mViewPager.setCurrentItem(currentItem - 1);
                        break;
                }
            });
        }

        mStepOneFragment = StepOneFragment.newInstance();
        mStepTwoFragment = StepTwoFragment.newInstance();
        mStepThreeFragment = StepThreeFragment.newInstance();
        mStepFourFragment = StepFourFragment.newInstance();

        CPFragmentPagerAdapter pagerAdapter = new CPFragmentPagerAdapter(getSupportFragmentManager(), FragmentStatePagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        pagerAdapter.addFragment(mStepOneFragment);
        pagerAdapter.addFragment(mStepTwoFragment);
        pagerAdapter.addFragment(mStepThreeFragment);
        pagerAdapter.addFragment(mStepFourFragment);

        mViewPager.setOffscreenPageLimit(4);
        mViewPager.setAdapter(pagerAdapter);
        // mViewPager.setCurrentItem(3);
    }

    @Override
    public void onNextButtonClicked() {
        UiUtils.hideKeyboard(this, getCurrentFocus());
        int currentItem = mViewPager.getCurrentItem();
        switch (currentItem) {
            case 0:
                getPresenter().checkEmailAndPasswordConditions(currentItem, mStepOneFragment.getEmail());
                break;
            case 1:
                getPresenter().checkForUserNameConditions(currentItem, mStepTwoFragment.getUserName());
                break;
            case 2:
                getPresenter().checkForMobileNumber(currentItem, mStepThreeFragment.getMobileNumber());
                break;
            case 3:
                // register user here
                AppExecutors.getInstance().networkIO().execute(this::registerUser);
                break;
        }
    }

    private void registerUser() {
        // fields from 1st fragment
        String email = mStepOneFragment.getEmail();
        String password = mStepOneFragment.getPassword();

        // fields from 2nd fragment
        String userName = mStepTwoFragment.getUserName();

        // fields from 3rd fragment
        String fullName = mStepThreeFragment.getFullName();
        String dob = mStepThreeFragment.getDob();
        String mobileNumber = mStepThreeFragment.getMobileNumber();
        String workPlace = mStepThreeFragment.getWorkPlace();
        String education = mStepThreeFragment.getEducation();
        String bio = mStepThreeFragment.getBio();
        String gender = mStepThreeFragment.getGender();

        // fields from 4th fragment
        boolean isCarOwner = mStepFourFragment.getIsCarOwner();
        String carModel = mStepFourFragment.getCarModel();
        String registrationPlate = mStepFourFragment.getRegistrationPlate();
        int seats = mStepFourFragment.getSeats();

        User user = new User(null, email, fullName, userName, null,
                dob, gender, registrationPlate, carModel, null, education, workPlace,
                bio, mobileNumber, 0, seats, 0, 0, isCarOwner);

        getPresenter().registerUser(user, password);
    }

    @Override
    public void gotoNextScreen(int currentItem) {
        mViewPager.setCurrentItem(currentItem + 1, true);
    }

    @Override
    public void setError(int errorType, int errorRes) {
        switch (errorType) {
            case AuthError.EMAIL:
                mStepOneFragment.setEmailError(getString(errorRes));
                break;
            case AuthError.MOBILE:
                mStepThreeFragment.setMobileNumberError(getString(errorRes));
                break;
            case AuthError.USERNAME:
                mStepTwoFragment.setUserNameError(getString(errorRes));
                break;
        }
    }

    @Override
    public void onSuccess() {
        SnackBarUtils.showSnackBarWithCallback(findViewById(android.R.id.content),
                getString(R.string.verify_email),
                requestMode -> {
                    LoginActivity.start(SignUpActivity.this);
                    SignUpActivity.this.finish();
                }, -1);
    }
}
package com.carpool.auth;

import androidx.annotation.StringRes;

import com.carpool.auth.register.AuthError;
import com.carpool.commons.CPView;

/**
 * Created by ranjit
 */
public interface AuthView extends CPView {
    void gotoNextScreen(int currentItem);

    void setError(@AuthError int errorType, @StringRes int errorRes);

    void showSnackBar(String message);

    void onSuccess();
}
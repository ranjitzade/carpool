package com.carpool.rides;

import androidx.annotation.NonNull;

import com.carpool.R;
import com.carpool.commons.CPMvpFragment;

/**
 * Created by ranjit
 */
public class RidesFragment extends CPMvpFragment<RidesPresenter, RidesView> implements RidesView {
    public static RidesFragment newInstance() {
        return new RidesFragment();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_rides;
    }

    @NonNull
    @Override
    public RidesPresenter providePresenter() {
        return new RidesPresenter();
    }

    @Override
    protected void onViewCreated() {

    }
}

package com.carpool.rides.create;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.RadioGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.carpool.R;
import com.carpool.utils.views.CPImageView;
import com.carpool.utils.views.CPTextView;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.google.android.material.radiobutton.MaterialRadioButton;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by ranjit
 */
public class CreateRideBottomSheetFragment extends BottomSheetDialogFragment {

    @BindView(R.id.textViewSource)
    protected CPTextView mTextViewSource;
    @BindView(R.id.imageViewMyLocation)
    protected CPImageView mImageViewMyLocation;
    @BindView(R.id.imageViewSwap)
    protected CPImageView mImageViewSwap;
    @BindView(R.id.textViewDestination)
    protected CPTextView mTextViewDestination;
    @BindView(R.id.textViewStartTime)
    protected CPTextView mTextViewStartTime;
    @BindView(R.id.textViewSeats)
    protected CPTextView mTextViewSeats;

    @BindView(R.id.radioGroupRides)
    protected RadioGroup mRadioGroupRides;
    @BindView(R.id.radioButtonRider)
    protected MaterialRadioButton mRadioButtonRider;
    @BindView(R.id.radioButtonDriver)
    protected MaterialRadioButton mRadioButtonDriver;

    @Nullable
    private CreateRidesListener mCreateRidesListener;
    private Unbinder unbinder;

    public static CreateRideBottomSheetFragment newInstance() {

        Bundle args = new Bundle();

        CreateRideBottomSheetFragment fragment = new CreateRideBottomSheetFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof CreateRidesListener) {
            mCreateRidesListener = (CreateRidesListener) context;
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.bottom_sheet_create_ride, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        unbinder = ButterKnife.bind(this, view);
        getDialog().setOnShowListener(dialog -> {
            try {
                BottomSheetDialog d = (BottomSheetDialog) dialog;
                FrameLayout bottomSheet = d.findViewById(R.id.design_bottom_sheet);
                if (bottomSheet == null)
                    return;
                bottomSheet.setBackground(null);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        });
    }

    @OnClick(R.id.imageViewMyLocation)
    protected void onGetMyLocationClicked() {
        if (mCreateRidesListener != null) {
            mCreateRidesListener.onMyLocationClicked();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    public interface CreateRidesListener {
        void onMyLocationClicked();
    }
}

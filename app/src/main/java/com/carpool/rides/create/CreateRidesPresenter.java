package com.carpool.rides.create;

import android.location.Address;
import android.location.Geocoder;
import android.location.Location;

import androidx.annotation.NonNull;

import com.carpool.CPApplication;
import com.carpool.commons.CPPresenter;
import com.carpool.models.Rides;
import com.carpool.models.User;
import com.carpool.utils.Constants;
import com.carpool.utils.logger.Log;
import com.google.android.gms.common.util.CollectionUtils;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import static com.carpool.utils.AppExecutors.getInstance;

/**
 * Created by ranjit
 */
public class CreateRidesPresenter extends CPPresenter<CreateRidesView> implements OnMapReadyCallback {

    private static final int DEFAULT_ZOOM = 15;
    private final LatLng mDefaultLocation = new LatLng(-33.8523341, 151.2106085);

    private GoogleMap mMap;
    private Rides mRides;

    @Override
    protected void onCreate() {
        super.onCreate();
        mRides = new Rides();
        getInstance().networkIO().execute(() -> {
            String uid = null;
            Log.e();
            if (FirebaseAuth.getInstance().getCurrentUser() != null) {
                uid = FirebaseAuth.getInstance().getCurrentUser().getUid();
            } else {
                CPApplication.getPreferences().getUserUuid();
            }
            DatabaseReference usersDb = FirebaseDatabase.getInstance().getReference().child(Constants.Firebase.USERS).child(uid);
            usersDb.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    Log.e();
                    User user = dataSnapshot.getValue(User.class);
                    if (user == null) return;
                    Log.e(user);
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    Log.e(databaseError.getMessage());

                }
            });
        });

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.mMap = googleMap;
        this.mMap.setTrafficEnabled(true);
        this.mMap.setMyLocationEnabled(true);
        this.mMap.getUiSettings().setCompassEnabled(false);
        this.mMap.getUiSettings().setAllGesturesEnabled(true);
        this.mMap.getUiSettings().setMyLocationButtonEnabled(false);
        this.mMap.getUiSettings().setZoomControlsEnabled(false);
        this.mMap.getUiSettings().setZoomGesturesEnabled(true);

        // Construct a FusedLocationProviderClient.
        sendToView(createRidesView -> {
            FusedLocationProviderClient mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(getViewOrThrow().getContext());
            Task<Location> lastLocation = mFusedLocationProviderClient.getLastLocation();
            lastLocation.addOnCompleteListener(task -> {
                if (task.isSuccessful()) {
                    // Set the map's camera position to the current location of the device.
                    Location mLastKnownLocation = task.getResult();
                    if (mLastKnownLocation == null) {
                        return;
                    }
                    LatLng latLng = new LatLng(mLastKnownLocation.getLatitude(),
                            mLastKnownLocation.getLongitude());
                    mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(
                            latLng, DEFAULT_ZOOM));
                    mMap.setMyLocationEnabled(true);
                    // mMap.addMarker(new MarkerOptions().position(latLng));
                } else {
                    Log.e("Current location is null. Using defaults.");
                    Log.e("Exception: %s", task.getException());
                    mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(mDefaultLocation, DEFAULT_ZOOM));
                    mMap.getUiSettings().setMyLocationButtonEnabled(false);
                }
            });
            lastLocation.addOnFailureListener(Throwable::printStackTrace);
        });
    }

    public void onLocationClicked() {
        if (mMap == null) {
            return;
        }
        Location myLocation = mMap.getMyLocation();
        getInstance().diskIO().execute(() -> {
            Geocoder geocoder = new Geocoder(getViewOrThrow().getContext(), Locale.getDefault());
            if (myLocation == null) {
                Log.e();
                return;
            }
            List<Address> locations = null;
            try {
                locations = geocoder.getFromLocation(myLocation.getLatitude(), myLocation.getLongitude(), 1);
            } catch (IOException e) {
                e.printStackTrace();
            }
            Log.e(locations);
            if (CollectionUtils.isEmpty(locations)) {
                return;
            }

            try {

                Address address = locations.get(0);
                if (address != null) {
                    if (address.getAddressLine(0) != null) {
                        sendToView(createRidesView -> createRidesView.setSourceAddress(address.getAddressLine(0)));
                        mRides.sourceAddress = address.getAddressLine(0);
                        mRides.sourceLatitude = address.getLatitude();
                        mRides.sourceLongitude = address.getLongitude();
                    }
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        });
    }

    public void addSeats() {
        if (mRides.seats >= 6) {
            return;
        }
        mRides.seats += 1;
        sendToView(createRidesView -> createRidesView.updateSeats(mRides.seats));
    }

    public void removeSeats() {
        if (mRides.seats <= 1) {
            return;
        }
        mRides.seats -= 1;
        sendToView(createRidesView -> createRidesView.updateSeats(mRides.seats));
    }

}

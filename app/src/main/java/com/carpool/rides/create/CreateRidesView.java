package com.carpool.rides.create;

import android.app.Activity;

import com.carpool.commons.CPView;

/**
 * Created by ranjit
 */
public interface CreateRidesView extends CPView {
    Activity getContext();

    void setSourceAddress(String address);

    void updateSeats(int seats);
}

package com.carpool.rides.create;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.RadioGroup;

import androidx.annotation.NonNull;

import com.carpool.R;
import com.carpool.commons.CPActivity;
import com.carpool.utils.DateUtils;
import com.carpool.utils.logger.Log;
import com.carpool.utils.views.CPImageView;
import com.carpool.utils.views.CPTextView;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.libraries.places.api.Places;
import com.google.android.material.radiobutton.MaterialRadioButton;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;

import java.util.Locale;

import butterknife.BindView;
import butterknife.OnClick;

public class CreateRidesActivity extends CPActivity<CreateRidesPresenter, CreateRidesView>
        implements CreateRidesView, PermissionListener {


    @BindView(R.id.textViewSource)
    protected CPTextView mTextViewSource;
    @BindView(R.id.imageViewMyLocation)
    protected CPImageView mImageViewMyLocation;
    @BindView(R.id.imageViewSwap)
    protected CPImageView mImageViewSwap;
    @BindView(R.id.textViewDestination)
    protected CPTextView mTextViewDestination;
    @BindView(R.id.textViewStartTime)
    protected CPTextView mTextViewStartTime;
    @BindView(R.id.textViewSeats)
    protected CPTextView mTextViewSeats;
    @BindView(R.id.radioGroupRides)
    protected RadioGroup mRadioGroupRides;
    @BindView(R.id.radioButtonRider)
    protected MaterialRadioButton mRadioButtonRider;
    @BindView(R.id.imageViewRemoveSeats)
    protected CPImageView mImageViewRemoveSeats;
    @BindView(R.id.imageViewAddSeats)
    protected CPImageView mImageViewAddSeats;

    public static void start(Context context) {
        Intent starter = new Intent(context, CreateRidesActivity.class);
        starter.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(starter);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_create_rides_new;
    }

    @NonNull
    @Override
    public CreateRidesPresenter providePresenter() {
        return new CreateRidesPresenter();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (!Places.isInitialized()) {
            Places.initialize(this, getString(R.string.google_maps_key), Locale.getDefault());
        }

        String dateAndTime = DateUtils.getDateAndTime(System.currentTimeMillis());
        mTextViewStartTime.setText(dateAndTime);
        mTextViewSeats.setText("1");


        Dexter.withActivity(this)
                .withPermission(Manifest.permission.ACCESS_COARSE_LOCATION)
                .withListener(this)
                .check();
    }

    @OnClick({R.id.imageViewRemoveSeats, R.id.imageViewAddSeats})
    protected void onSeatsClicked(View view) {
        if (view.getId() == R.id.imageViewRemoveSeats) {
            getPresenter().removeSeats();
        } else if (view.getId() == R.id.imageViewAddSeats) {
            getPresenter().addSeats();
        }
    }

    @OnClick(R.id.imageViewMyLocation)
    public void onMyLocationClicked() {
        try {
            getPresenter().onLocationClicked();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onPermissionGranted(PermissionGrantedResponse response) {
        Log.e();
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        if (mapFragment != null) {
            mapFragment.getMapAsync(getPresenter());
        }
    }

    @Override
    public void onPermissionDenied(PermissionDeniedResponse response) {
        Log.e();
        // todo add compulsory location here
    }

    @Override
    public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {
        Log.e();
        token.continuePermissionRequest();
    }

    @Override
    public Activity getContext() {
        return this;
    }

    @Override
    public void setSourceAddress(String address) {
        mTextViewSource.setText(address);
    }

    @Override
    public void updateSeats(int seats) {
        mTextViewSeats.setText(String.valueOf(seats));
    }
}
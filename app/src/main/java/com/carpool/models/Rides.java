package com.carpool.models;

import com.carpool.utils.Constants;
import com.google.firebase.database.PropertyName;

import static com.carpool.utils.Constants.Firebase.MOBILE_NUMBER;

/**
 * Created by ranjit
 */
public class Rides {
    @PropertyName(Constants.Firebase.USER_UUID)
    public String userUuid;
    public String username;
    @PropertyName(Constants.Firebase.PROFILE_PHOTO)
    public String profilePhoto;
    @PropertyName(MOBILE_NUMBER)
    public String mobileNumber;

    @PropertyName(Constants.Firebase.REGISTRATION_PLATE)
    public String registrationPlate;
    public String car;
    @PropertyName(Constants.Firebase.CAR_PHOTO)
    public String carPhoto;

    @PropertyName(Constants.Firebase.SOURCE_LATITUDE)
    public double sourceLatitude;
    @PropertyName(Constants.Firebase.SOURCE_LONGITUDE)
    public double sourceLongitude;
    @PropertyName(Constants.Firebase.SOURCE_ADDRESS)
    public String sourceAddress;

    @PropertyName(Constants.Firebase.DESTINATION_LATITUDE)
    public double destinationLatitude;
    @PropertyName(Constants.Firebase.DESTINATION_LONGITUDE)
    public double destinationLongitude;
    @PropertyName(Constants.Firebase.DESTINATION_ADDRESS)
    public String destinationAddress;

    public int seats;
    @PropertyName(Constants.Firebase.TIME_IN_MILLIS)
    public long timeInMillis;
}
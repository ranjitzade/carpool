package com.carpool.models;

import com.carpool.utils.Constants;
import com.google.firebase.database.PropertyName;

import static com.carpool.utils.Constants.Firebase.MOBILE_NUMBER;
import static com.carpool.utils.Constants.Firebase.USERNAME;

/**
 * Created by ranjit
 */
public class User {
    @PropertyName(Constants.Firebase.USER_UUID)
    public String userUuid;
    public String email;
    @PropertyName(Constants.Firebase.FULL_NAME)
    public String fullName;
    @PropertyName(USERNAME)
    public String username;
    @PropertyName(Constants.Firebase.PROFILE_PHOTO)
    public String profilePhoto;
    public String dob;
    public String gender;
    @PropertyName(Constants.Firebase.REGISTRATION_PLATE)
    public String registrationPlate;
    public String car;
    @PropertyName(Constants.Firebase.CAR_PHOTO)
    public String carPhoto;
    public String education;
    public String work;
    public String bio;
    @PropertyName(MOBILE_NUMBER)
    public String mobileNumber;
    @PropertyName("completed_rides")
    public int completedRides;
    public int seats;
    @PropertyName("user_rating")
    public int userRating;
    public int points;
    @PropertyName(Constants.Firebase.CAR_OWNER)
    public boolean car_owner;

    public User(String userUuid, String email, String fullName, String username, String profilePhoto, String dob, String gender, String registrationPlate, String car, String carPhoto, String education, String work, String bio, String mobileNumber, int completedRides, int seats, int userRating, int points, boolean car_owner) {
        this.userUuid = userUuid;
        this.email = email;
        this.fullName = fullName;
        this.username = username;
        this.profilePhoto = profilePhoto;
        this.dob = dob;
        this.gender = gender;
        this.registrationPlate = registrationPlate;
        this.car = car;
        this.carPhoto = carPhoto;
        this.education = education;
        this.work = work;
        this.bio = bio;
        this.mobileNumber = mobileNumber;
        this.completedRides = completedRides;
        this.seats = seats;
        this.userRating = userRating;
        this.points = points;
        this.car_owner = car_owner;
    }

    public User() {
    }

    @Override
    public String toString() {
        return "User{" +
                "userUuid='" + userUuid + '\'' +
                ", email='" + email + '\'' +
                ", fullName='" + fullName + '\'' +
                ", username='" + username + '\'' +
                ", profilePhoto='" + profilePhoto + '\'' +
                ", dob='" + dob + '\'' +
                ", gender='" + gender + '\'' +
                ", registrationPlate='" + registrationPlate + '\'' +
                ", car='" + car + '\'' +
                ", carPhoto='" + carPhoto + '\'' +
                ", education='" + education + '\'' +
                ", work='" + work + '\'' +
                ", bio='" + bio + '\'' +
                ", mobileNumber='" + mobileNumber + '\'' +
                ", completedRides=" + completedRides +
                ", seats=" + seats +
                ", userRating=" + userRating +
                ", points=" + points +
                ", car_owner=" + car_owner +
                '}';
    }
}

package com.carpool.home;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.carpool.R;
import com.carpool.commons.CPActivity;
import com.carpool.commons.CPFragmentPagerAdapter;
import com.carpool.commons.CommonPresenter;
import com.carpool.commons.CommonView;
import com.carpool.home.maps.HomeFragment;
import com.carpool.profile.ProfileFragment;
import com.carpool.rides.RidesFragment;
import com.carpool.utils.logger.Log;
import com.carpool.utils.views.NonSwipeableViewPager;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import butterknife.BindView;

/**
 * Created by ranjit
 */
public class HomeActivity extends CPActivity<CommonPresenter, CommonView> implements CommonView {

    @BindView(R.id.view_pager)
    protected NonSwipeableViewPager mViewPager;
    @BindView(R.id.bottomNavigationBar)
    protected BottomNavigationView mBottomNavigationBar;

    public static void start(Context context) {
        Intent starter = new Intent(context, HomeActivity.class);
        starter.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(starter);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_main;
    }

    @NonNull
    @Override
    public CommonPresenter providePresenter() {
        return new CommonPresenter();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        CPFragmentPagerAdapter pagerAdapter = new CPFragmentPagerAdapter(getSupportFragmentManager(), FragmentStatePagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        pagerAdapter.addFragment(RidesFragment.newInstance());
        pagerAdapter.addFragment(HomeFragment.newInstance());
        pagerAdapter.addFragment(ProfileFragment.newInstance());

        mViewPager.setOffscreenPageLimit(3);
        mViewPager.setAdapter(pagerAdapter);

        mBottomNavigationBar.setOnNavigationItemSelectedListener(item -> {
            switch (item.getItemId()) {
                case R.id.rides:
                    mViewPager.setCurrentItem(0, false);
                    break;
                case R.id.profile:
                    mViewPager.setCurrentItem(2, false);
                    break;
                case R.id.home:
                    mViewPager.setCurrentItem(1, false);
            }
            return true;
        });

        mBottomNavigationBar.setOnNavigationItemReselectedListener(item -> {
            Log.e("item reselected");
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        mBottomNavigationBar.setOnNavigationItemSelectedListener(null);
        mBottomNavigationBar.setOnNavigationItemReselectedListener(null);
    }
}

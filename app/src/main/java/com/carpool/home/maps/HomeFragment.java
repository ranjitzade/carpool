package com.carpool.home.maps;

import androidx.annotation.NonNull;

import com.carpool.R;
import com.carpool.commons.CPMvpFragment;
import com.carpool.utils.logger.Log;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.AutocompleteSupportFragment;
import com.google.android.libraries.places.widget.listener.PlaceSelectionListener;

import java.util.Arrays;

/**
 * Created by ranjit
 */
public class HomeFragment extends CPMvpFragment<HomePresenter, HomeView> implements HomeView, OnMapReadyCallback {

    private GoogleMap mGoogleMap;

    public static HomeFragment newInstance() {
        return new HomeFragment();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_home;
    }

    @NonNull
    @Override
    public HomePresenter providePresenter() {
        return new HomePresenter();
    }

    @Override
    protected void onViewCreated() {
        if (getActivity() == null) {
            return;
        }
        Places.initialize(getActivity(), getString(R.string.google_maps_key));
        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager()
                .findFragmentById(R.id.mapFragment);
        if (mapFragment != null) {
            mapFragment.getMapAsync(this);
        }

        // Initialize the AutocompleteSupportFragment.
        AutocompleteSupportFragment autocompleteFragment = (AutocompleteSupportFragment)
                getChildFragmentManager().findFragmentById(R.id.autocomplete_fragment);

        // Specify the types of place data to return.
        if (autocompleteFragment != null) {
            autocompleteFragment.setPlaceFields(Arrays.asList(Place.Field.ID, Place.Field.NAME));

            // autocompleteFragment.setTypeFilter(TypeFilter.ADDRESS);

            // Set up a PlaceSelectionListener to handle the response.
            autocompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
                @Override
                public void onPlaceSelected(@NonNull Place place) {
                    // TODO: Get info about the selected place.
                    Log.i("Place: " + place.getName() + ", " + place.getId());
                    if (place.getLatLng() != null) {
                        mGoogleMap.addMarker(new MarkerOptions().position(place.getLatLng()));
                    }
                }

                @Override
                public void onError(@NonNull Status status) {
                    // TODO: Handle the error.
                    Log.i("An error occurred: " + status);
                }
            });
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.mGoogleMap = googleMap;
        this.mGoogleMap.setTrafficEnabled(true);
        this.mGoogleMap.setMyLocationEnabled(false);
        this.mGoogleMap.getUiSettings().setAllGesturesEnabled(true);
        this.mGoogleMap.getUiSettings().setZoomControlsEnabled(false);
        this.mGoogleMap.getUiSettings().setZoomGesturesEnabled(true);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mGoogleMap.clear();
        mGoogleMap = null;
    }
}

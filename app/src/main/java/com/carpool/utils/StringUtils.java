package com.carpool.utils;

import android.text.TextUtils;

/**
 * Created by ranjit
 */
public class StringUtils {
    private StringUtils() {
        throw new AssertionError();
    }

    /**
     * Check whether a string is not NULL, empty or "NULL", "null", "Null"
     */
    public static boolean isNotEmpty(String str) {
        boolean flag = true;
        if (str != null) {
            str = str.trim();
            if (str.length() == 0 || str.equalsIgnoreCase("null")) {
                flag = false;
            }
        } else {
            flag = false;
        }
        return flag;
    }

    public static boolean isEmpty(String str) {
        return !isNotEmpty(str);
    }

    /**
     * call method to email validation
     */
    public static boolean isValidEmail(CharSequence target) {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

}

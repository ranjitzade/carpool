package com.carpool.utils;

import android.content.res.Resources;
import android.view.View;

import com.carpool.R;
import com.google.android.material.snackbar.Snackbar;

public class SnackBarUtils {

    private SnackBarUtils() {
        throw new AssertionError();
    }

    public static void showSnackBar(View view, String message) {
        if (view != null && StringUtils.isNotEmpty(message)) {
            Snackbar.make(view, message, Snackbar.LENGTH_LONG).show();
        }
    }

    public static void showNoInternetSnackBar(View view) {
        showSnackBar(view, Resources.getSystem().getString(R.string.no_internet_connection));
    }

    public static void showSnackBarWithCallback(View view, String message, final SnackBarCallback snackBarCallback, final int requestMode) {
        if (view == null || StringUtils.isEmpty(message)) {
            return;
        }

        Snackbar make = Snackbar.make(view, message, Snackbar.LENGTH_SHORT);
        make.addCallback(new Snackbar.Callback() {
            @Override
            public void onDismissed(Snackbar transientBottomBar, int event) {
                super.onDismissed(transientBottomBar, event);
                snackBarCallback.onSnackBarDismissed(requestMode);
            }
        });
        make.show();
    }

    public static void showSnackBarWithAction(View view, String message, String action, final SnackBarActionListener snackBarActionListener, final int requestMode) {
        if (view == null || StringUtils.isEmpty(message)) {
            return;
        }
        final Snackbar snackbar = Snackbar.make(view, message, Snackbar.LENGTH_INDEFINITE);
        snackbar.setAction(action, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                snackBarActionListener.onClick(v, requestMode);
                snackbar.dismiss();
            }
        });
        snackbar.show();
    }

    public interface SnackBarCallback {
        void onSnackBarDismissed(int requestMode);
    }

    public interface SnackBarActionListener {
        void onClick(View view, int requestMode);
    }
}
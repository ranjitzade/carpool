package com.carpool.utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by ranjit
 */
public class CPPreferences implements Constants.Preferences {

    private final SharedPreferences mSharedPreferences;

    public CPPreferences(Context context) {
        this.mSharedPreferences = context.getSharedPreferences(CP_PREFERENCE, Context.MODE_PRIVATE);
    }


    public void setLoggedIn(boolean isLoggedIn) {
        mSharedPreferences.edit().putBoolean(IS_LOGGED_IN, isLoggedIn).apply();
    }

    public boolean isUserLoggedIn() {
        return mSharedPreferences.getBoolean(IS_LOGGED_IN, false);
    }

    public String getEmailId() {
        return mSharedPreferences.getString(EMAIL, "");
    }

    public void setEmailId(String email) {
        mSharedPreferences.edit().putString(EMAIL, email).apply();
    }

    public String getUserUuid() {
        return mSharedPreferences.getString(USER_UUID, "");
    }

    public void setUserUuid(String userUuid) {
        mSharedPreferences.edit().putString(USER_UUID, userUuid).apply();
    }

    public void logout() {
        mSharedPreferences.edit().clear().apply();
    }
}
package com.carpool.utils;

/**
 * Created by ranjit
 */
public interface Constants {

    interface Firebase {
        String USERS = "users";

        String USERNAME = "username";
        String MOBILE_NUMBER = "mobile_number";
        String USER_UUID = "user_uuid";
        String SOURCE_LATITUDE = "source_latitude";
        String SOURCE_LONGITUDE = "source_longitude";
        String DESTINATION_LATITUDE = "destination_latitude";
        String DESTINATION_LONGITUDE = "destination_longitude";
        String PROFILE_PHOTO = "profile_photo";
        String DESTINATION_ADDRESS = "destination_address";
        String SOURCE_ADDRESS = "source_address";
        String TIME_IN_MILLIS = "time_in_millis";
        String CAR_PHOTO = "car_photo";
        String REGISTRATION_PLATE = "registration_plate";
        String FULL_NAME = "full_name";
        String CAR_OWNER = "car_owner";
    }

    interface Preferences {
        String CP_PREFERENCE = "cp_preference";
        String IS_LOGGED_IN = "is_logged_in";
        String EMAIL = "email";
        String USER_UUID = "user_uuid";
    }
}

package com.carpool.utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

/**
 * Created by ranjit
 */
public class DateUtils {
    public static final SimpleDateFormat SIMPLE_DATE_FORMAT = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());
    public static final SimpleDateFormat DATE_AND_TIME = new SimpleDateFormat("dd MMM, hh:mm a", Locale.getDefault());

    public static String getDateInString(long milliSeconds) {
        // Create a calendar object that will convert the date and time value in milliseconds to date.
        return getDateInString(milliSeconds, SIMPLE_DATE_FORMAT);
    }

    public static String getDateAndTime(long milliSeconds) {
        return getDateInString(milliSeconds, DATE_AND_TIME);
    }

    public static String getDateInString(long milliSeconds, SimpleDateFormat SIMPLE_DATE_FORMAT) {
        // Create a calendar object that will convert the date and time value in milliseconds to date.
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliSeconds);
        return SIMPLE_DATE_FORMAT.format(calendar.getTime());
    }


}

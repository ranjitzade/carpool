package com.carpool.utils.views;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;

import androidx.annotation.DrawableRes;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.core.content.ContextCompat;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

/**
 * Created by ranjit
 */
public class CPImageView extends AppCompatImageView {
    public CPImageView(Context context) {
        super(context);
    }

    public CPImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CPImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void load(String url) {
        Glide.with(this).load(url).into(this);
    }

    public void load(Drawable drawable) {
        Glide.with(this).load(drawable).into(this);
    }

    public void load(@DrawableRes int drawable) {
        RequestOptions requestOptions = new RequestOptions();
        Glide.with(this)
                .load(ContextCompat.getDrawable(getContext(), drawable))
                .into(this);
    }
}

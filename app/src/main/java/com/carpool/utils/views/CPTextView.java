package com.carpool.utils.views;

import android.content.Context;
import android.util.AttributeSet;

import com.google.android.material.textview.MaterialTextView;

/**
 * Created by ranjit
 */
public class CPTextView extends MaterialTextView {
    public CPTextView(Context context) {
        super(context);
    }

    public CPTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CPTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }
}

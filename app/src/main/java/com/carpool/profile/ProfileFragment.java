package com.carpool.profile;

import androidx.annotation.NonNull;

import com.carpool.R;
import com.carpool.commons.CPMvpFragment;

/**
 * Created by ranjit
 */
public class ProfileFragment extends CPMvpFragment<ProfilePresenter, ProfileView> implements ProfileView {
    public static ProfileFragment newInstance() {
        return new ProfileFragment();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_profile;
    }

    @NonNull
    @Override
    public ProfilePresenter providePresenter() {
        return new ProfilePresenter();
    }

    @Override
    protected void onViewCreated() {

    }
}

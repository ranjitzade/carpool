package com.carpool.commons;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;

import androidx.annotation.LayoutRes;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.NavUtils;
import androidx.core.app.TaskStackBuilder;

import com.carpool.R;
import com.carpool.utils.SnackBarUtils;
import com.carpool.utils.UiUtils;

import net.grandcentrix.thirtyinch.TiActivity;
import net.grandcentrix.thirtyinch.TiPresenter;
import net.grandcentrix.thirtyinch.TiView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public abstract class CPActivity<P extends TiPresenter<V>, V extends TiView> extends TiActivity<P, V> {

    @Nullable
    @BindView(R.id.toolbar)
    protected Toolbar toolbar;
    private ProgressDialog mProgressDialog;
    private Unbinder unbinder;

    public static void navigateUpOrBack(AppCompatActivity currentActivity,
                                        Class<? extends AppCompatActivity> syntheticParentActivity) {
        // Retrieve parent activity from AndroidManifest.
        Intent intent = NavUtils.getParentActivityIntent(currentActivity);

        // Synthesize the parent activity when a natural one doesn't exist.
        if (intent == null && syntheticParentActivity != null) {
            try {
                intent = NavUtils.getParentActivityIntent(currentActivity, syntheticParentActivity);
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }
        }

        if (intent == null) {
            // No parent defined in manifest. This indicates the activity may be used by
            // in multiple flows throughout the app and doesn't have a strict parent. In
            // this case the navigation up button should act in the same manner as the
            // back button. This will result in users being forwarded back to other
            // applications if currentActivity was invoked from another application.
            currentActivity.onBackPressed();
        } else {
            if (NavUtils.shouldUpRecreateTask(currentActivity, intent)) {
                // Need to synthesize a backstack since currentActivity was probably invoked by a
                // different app. The preserves the "Up" functionality within the app according to
                // the activity hierarchy defined in AndroidManifest.xml via parentActivity
                // attributes.
                TaskStackBuilder builder = TaskStackBuilder.create(currentActivity);
                builder.addNextIntentWithParentStack(intent);
                builder.startActivities();
            } else {
                // Navigate normally to the manifest defined "Up" activity.
                NavUtils.navigateUpTo(currentActivity, intent);
            }
        }
    }

    @LayoutRes
    protected abstract int getLayoutId();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutId());
        unbinder = ButterKnife.bind(this);
    }

    protected void setToolbar(String title, boolean isHomeAsUpEnabled) {
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            ActionBar ab = getSupportActionBar();
            if (ab != null) {
                // ab.setLogo(ContextCompat.getDrawable(this, R.mipmap.ic_launcher));
                ab.setDisplayHomeAsUpEnabled(isHomeAsUpEnabled);
                ab.setTitle(title);
            }
            toolbar.setTitle(title);
            if (isHomeAsUpEnabled) {
                toolbar.setNavigationOnClickListener(view -> navigateUpOrBack(CPActivity.this, null));
            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        try {
            unbinder.unbind();
            Runtime.getRuntime().gc();
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

    public void showSnackBar(String message) {
        try {
            SnackBarUtils.showSnackBar(findViewById(android.R.id.content), message);
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

    public void showSnackBar(int resId) {
        showSnackBar(getString(resId));
    }

    public void showNoInternetSnackBar() {
        showSnackBar(getString(R.string.no_internet_connection));
    }

    public void showProgressDialog(boolean showDialog) {
        if (showDialog) {
            mProgressDialog = UiUtils.showLoadingDialog(this);
        } else {
            UiUtils.hideProgressDialog(this, mProgressDialog);
        }
    }

}

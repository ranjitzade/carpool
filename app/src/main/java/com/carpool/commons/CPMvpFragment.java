package com.carpool.commons;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.carpool.utils.UiUtils;

import net.grandcentrix.thirtyinch.TiFragment;

import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by ranjit
 */
public abstract class CPMvpFragment<P extends CPPresenter<V>, V extends CPView> extends TiFragment<P, V> {
    private Unbinder unbinder;
    private ProgressDialog mProgressDialog;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        return inflater.inflate(getLayoutId(), container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        unbinder = ButterKnife.bind(this, view);
        onViewCreated();
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    public void showProgressDialog(boolean showProgress) {
        if (showProgress) {
            mProgressDialog = UiUtils.showLoadingDialog(getContext());
        } else {
            UiUtils.hideProgressDialog(getContext(), mProgressDialog);
        }
    }

    @LayoutRes
    protected abstract int getLayoutId();

    protected abstract void onViewCreated();
}
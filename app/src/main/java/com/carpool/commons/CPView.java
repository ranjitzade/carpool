package com.carpool.commons;

import net.grandcentrix.thirtyinch.TiView;

/**
 * Created by ranjit
 */
public interface CPView extends TiView {
    void showProgressDialog(boolean showProgress);
}
